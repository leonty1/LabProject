﻿using Lab.Dto;
using NHibernate;
using NHibernate.Linq;

public class Repository
{
    private readonly ISession session;
    public Repository(NHibernate.ISession session)
    {
        this.session = session;
    }
    
    /// <summary>
    /// создание заказа
    /// </summary>
    /// <param name="researcheIds"></param>
    /// <param name="createdDate"></param>
    /// <param name="fio"></param>
    /// <returns></returns>
    public async Task<long> CreateOrderAsync(List<long> researcheIds, string fio)
    {
        
        using (var transaction = session.BeginTransaction())
        {
            var order = new Order()
            {
                FIO = fio

            };
            //сохранение заказа в бд
            await session.SaveAsync(order);

            // создание списка контейнеров согласно правилам
            var containers = await session.Query<Rule>()
                .Where(x => researcheIds.Contains(x.ResearcheId))
                .Select(x => x.Container)
                .ToListAsync();

            // создание образца
            var samples = containers.Select(x => new Sample
            {
                Container = x,
                ContainerId = x.Id,
                OrderId = order.Id,
            }).ToList();

            //сохранение заказа в бд
            foreach (var item in samples)
            {
                await session.SaveAsync(item);
            }

            await transaction.CommitAsync();
            return order.Id;
        }       
    }

    /// <summary>
    /// По Order id получаем список образцов для этого заказа
    /// </summary>
    /// <param name="orderId"></param>
    /// <returns></returns>
    public async Task<List<SamplesDto>> GetSamplesAsync(long orderId)
    {
        var samples = await session.Query<Sample>()
            .Where(x => x.OrderId == orderId)
            .Select(x => new SamplesDto
            {
                Number = x.Number,
                Id = x.Id,
                ContainerName = x.Container.Name
            })
            .ToListAsync();

        return samples;
    }

    /// <summary>
    /// получаем список исследований
    /// </summary>
    /// <returns></returns>
    public async Task<List<Researche>> GetResearchesAsync()
    {
        var samples = await session.Query<Researche>()
            .ToListAsync();

        return samples;
    }

    /// <summary>
    /// получаем список заказов
    /// </summary>
    /// <returns></returns>
    public async Task<List<Order>> GetOrderListAsync()
    {
        var samples = await session.Query<Order>()
            .ToListAsync();

        return samples;
    }

    /// <summary>
    /// присвоение номера образцу
    /// </summary>
    /// <param name="number"></param>
    /// <param name="orderId"></param>
    public async Task NumberСreationAsync(int number, long sampleId)
    {
        using var transaction = session.BeginTransaction();
        var sample = await session.Query<Sample>()
            .Where(x => x.Id == sampleId)
            .FirstOrDefaultAsync();
        sample.Number = number;
        await session.SaveOrUpdateAsync(sample);
        await transaction.CommitAsync();
    }
}
