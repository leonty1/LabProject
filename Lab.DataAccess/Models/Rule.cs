﻿public class Rule
{
    public virtual long Id { get; set; }
    public virtual long ContainerId { get; set; }
    public virtual long ResearcheId { get; set; }

    public virtual Container Container { get; set; }
}
