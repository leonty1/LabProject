﻿public class Container
{
    public virtual long Id { get; set; }
    public virtual string Name { get; set; }

    public virtual IList<Sample> Samples { get; set; } = new List<Sample>();
    public virtual IList<Rule> Rules { get; set; } = new List<Rule>();
}
