﻿public class Sample
{
    public virtual long Id { get; set; }
    public virtual long OrderId { get; set; }
    public virtual int Number { get; set; }
    public virtual long ContainerId { get; set; }
    

    public virtual Container Container { get; set; }
}
