﻿using FluentNHibernate.Mapping;

public class ContainerMap : ClassMap<Container>
{
    public ContainerMap()
    {
        Table("containers");
        Id(x => x.Id, "id");
        Map(x => x.Name, "name");
        HasMany(x => x.Samples)
         .KeyColumn("container_id");
        HasMany(x => x.Rules)
        .KeyColumn("container_id");
    }
}
