﻿using FluentNHibernate.Mapping;

public class ResearcheMap : ClassMap<Researche>
{
    public ResearcheMap()
    {
        Table("researches");
        Id(x => x.Id, "id");
        Map(x => x.Name, "name");
        Map(x => x.Price, "price");
    }
}
