﻿using FluentNHibernate.Mapping;

public class OrderMap: ClassMap<Order>
{
    public OrderMap()
    {
        Id(x => x.Id, "id");
        Map(x => x.FIO, "fio");
        Table("orders");
    }
}
