﻿using FluentNHibernate.Mapping;

public class RuleMap : ClassMap<Rule>
{
    public RuleMap()
    {
        Table("rules");
        Id(x => x.Id, "id");
        Map(x => x.ResearcheId, "researche_id");
        References(x => x.Container)
            .Column("container_id");
    }
}
