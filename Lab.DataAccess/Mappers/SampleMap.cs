﻿using FluentNHibernate.Mapping;

public class SampleMap : ClassMap<Sample>
{
    public SampleMap()
    {
        Table("samples");
        Id(x => x.Id, "id");
        Map(x => x.OrderId, "order_id");
        References(x => x.Container)
            .Column("container_id");
        Map(x => x.Number, "number");
       
    }
}
