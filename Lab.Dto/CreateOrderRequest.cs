﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab.Dto
{
    public class CreateOrderRequest
    {
        public List<long> ResearcheIds { get; set; }
        
        public string Fio { get; set; }
    }
}
