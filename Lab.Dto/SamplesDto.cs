﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab.Dto
{
    public class SamplesDto
    {
        public virtual long Id { get; set; }
        public virtual int Number { get; set; }
        public virtual string ContainerName { get; set; }

    }
}
