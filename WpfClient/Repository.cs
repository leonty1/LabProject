﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Net.Http.Json;
using Lab.Dto;
using System.Threading;
using System.Windows.Controls;

namespace WpfClient
{
    public class Repository
    {
        /// <summary>
        /// создание  HttpClient
        /// </summary>
        /// <returns></returns>
        private HttpClient GetClient()
        {
            var client = new HttpClient();
            client.BaseAddress = new Uri("https://localhost:44391/");
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            return client;
        }

        /// <summary>
        /// преобразует передаваемый класс в параметры api
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="data"></param>
        /// <returns></returns>
        private StringContent ConvertToStringContent<T>(T data)
        {
            return new StringContent(JsonConvert.SerializeObject(data), Encoding.UTF8, "application/json");
        }
        /// <summary>
        /// отправляет запрос на создание заказа в Api
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<long> CreateOrder(CreateOrderRequest request)
        {
            /// using закрывает поток после выполнения задачи
            using var client = GetClient();
            var response = await client.PostAsync($"create", ConvertToStringContent(request));

            if (response.IsSuccessStatusCode)
                return await response.Content.ReadFromJsonAsync<long>();
            return 0;
        }

        /// <summary>
        /// загрузка списка исследований
        /// </summary>
        /// <returns></returns>
        public async Task<List<ResearcheDto>> GetResearchesListAsync()
        {
            using var client = GetClient();
            var response = await client.GetAsync($"GetResearchesList");

            if (response.IsSuccessStatusCode)
                return await response.Content.ReadFromJsonAsync<List<ResearcheDto>>();
            return new List<ResearcheDto>();
        }

        /// <summary>
        /// загрузка списка заказов
        /// </summary>
        /// <returns></returns>
        public async Task<List<OrderDto>> GetOrderListAsync()
        {
            using var client = GetClient();
            var response = await client.GetAsync($"GetOrderList");

            if (response.IsSuccessStatusCode)
                return await response.Content.ReadFromJsonAsync<List<OrderDto>>();
            return new List<OrderDto>();
        }


        /// <summary>
        /// загрузка списка образцов по order ID
        /// </summary>
        /// <returns></returns>
        public async Task<List<SamplesDto>> GetSamplesListAsync(long orderId)
        {
            using var client = GetClient();
            var response = await client.GetAsync($"GetSamplesList?orderId={orderId}");

            if (response.IsSuccessStatusCode)
                return await response.Content.ReadFromJsonAsync<List<SamplesDto>>();
            return new List<SamplesDto>();
        }

        /// <summary>
        /// присвоение номера выбранному образцу
        /// </summary>
        /// <param name="sampleId"></param>
        /// <param name="number"></param>
        public async void CreateNumberAsync(long sampleId, int number)
        {
            using var client = GetClient();
            var response = await client.PostAsync($"CreateNumber?sampleId={sampleId}&number={number}", null);
        }
    }
}
