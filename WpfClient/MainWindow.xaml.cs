﻿using Lab.Dto;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfClient
{
    public partial class MainWindow : Window
    {
        Repository repository = new();
        public MainWindow()
        {
            InitializeComponent();           
        }

        private BindingList<Researce> _researces = new();
        private BindingList<OrderDto> _orders = new();
        private BindingList<SamplesDto> _samples = new();

        //создание заказа 
        private async void CreateOrderButton_Click(object sender, RoutedEventArgs e)
        {
            // вытаскиваем Id исследований с активными чекбоксами
            var researceIds = _researces.Where(x => x.IsCheck).Select(x => x.Id).ToList();
            var fio = FIO.Text;
            //проверка включены ли чек боксы и заполнено ли поле ФИО
            if (researceIds.Count == 0 || string.IsNullOrWhiteSpace(fio))
                return;
            
            // собираем экземпляр класса createOrder
            CreateOrderRequest createOrder = new()
            {
                ResearcheIds = researceIds,
                Fio = fio
            };
            //передаем экземляр в репозиторий, после зоздания заказа метод возвращает его  Id 
            var orderId = await repository.CreateOrder(createOrder);

            // записываем данные о новом заказе в  BindingList<OrderDto> _orders, тем самым отображая их в визуалке 
            _orders.Add(new OrderDto { FIO = fio, Id = orderId });

            // очистка text box FIO
            FIO.Clear();
        }

        /// <summary>
        /// Список образцов выделенного заказа
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void OrdersListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (OrdersListView.SelectedItem is OrderDto currentOrder && currentOrder != null)
            {
                var result = await repository.GetSamplesListAsync(currentOrder.Id);
                _samples = new(result);
                // запись образца в SamplesList, тем самым отображая его в визуалке 
                SamplesList.ItemsSource = _samples;
            }
        }

        private async void Window_Loaded(object sender, RoutedEventArgs e)
        {
            //запись в BindingList _researces при старте прогаммы
            var response = await repository.GetResearchesListAsync();

            _researces = new BindingList<Researce>(response.Select(x => new Researce
            {
                Id = x.Id,
                Name = x.Name,
                Price = x.Price
            }).ToList());

            // загруза списка исследований в DataGrid1
            DataGrid1.ItemsSource = _researces;

            // получаем список заказов
            var orderList = await repository.GetOrderListAsync();
            _orders = new BindingList<OrderDto>(orderList.Select(x => new OrderDto
            {
                Id = x.Id,
                FIO = x.FIO
            }).ToList());
            // загруза списка заказоав в  OrdersListView
            OrdersListView.ItemsSource = _orders;

        }
        /// <summary>
        /// присваиваем номер выделенному образцу
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void SetNumberButton_Click(object sender, RoutedEventArgs e)
        {
            if (SamplesList.SelectedItem is SamplesDto currentSample && currentSample != null)
            {
                //если текст из Number парсится в int 
                if (int.TryParse(Number.Text, out var number))
                {
                    //то присваиваем полю Number номер
                    currentSample.Number = number;
                    repository.CreateNumberAsync(currentSample.Id, number);
                    // очистка text box Number
                    Number.Clear();
                    //обнуление списка образцов
                    SamplesList.ItemsSource = null;
                    //установка значения после изменения номера образца
                    SamplesList.ItemsSource = _samples;
                }
            }
        }
    }
}
