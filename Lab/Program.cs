using Lab.DataAccess;
using Lab.Dto;
using Microsoft.AspNetCore.Mvc;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddScoped<Repository>();
builder.Services.AddNHibernate(builder.Configuration);

var app = builder.Build();

if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.MapGet("/", (NHibernate.ISession s) =>
{
    using (s.BeginTransaction())
    { 
        return s.CreateCriteria<Order>().List();
    }
}).WithOpenApi();

/// <summary>
/// �������� ������
/// </summary>
/// <param name="researcheIds"></param>
/// <param name="createdDate"></param>
/// <param name="fio"></param>
/// <returns></returns>
app.MapPost("/create", (CreateOrderRequest data, [FromServices] Repository repository) =>
    repository.CreateOrderAsync(data.ResearcheIds, data.Fio))
    .WithName("Create order")
    .WithOpenApi();

/// <summary>
/// �� Order id �������� ������ �������� ��� ����� ������
/// </summary>
/// <param name="orderId"></param>
/// <returns></returns>
app.MapGet("/GetSamplesList", (long orderId, [FromServices] Repository repository) =>
    repository.GetSamplesAsync(orderId))
    .WithName("GetSamplesList")
    .WithOpenApi();

/// <summary>
/// ���������� ������ �������
/// </summary>
/// <param name="researcheIds"></param>
/// <param name="createdDate"></param>
/// <param name="fio"></param>
/// <returns></returns>
app.MapPost("/CreateNumber", (int number, long sampleId, [FromServices] Repository repository) =>
    repository.Number�reationAsync(number, sampleId))
    .WithName("CreateNumber")
    .WithOpenApi();

/// <summary>
/// �������� ������ ������������
/// </summary>
/// <param name="researcheIds"></param>
/// <param name="createdDate"></param>
/// <param name="fio"></param>
/// <returns></returns>
app.MapGet("/GetResearchesList", async ([FromServices] Repository repository) =>
    (await repository.GetResearchesAsync()).Select(x => new ResearcheDto
    {
        Id = x.Id, Name = x.Name, Price = x.Price
    }).ToList())
    .WithName("GetResearchesList")
    .WithOpenApi();

/// <summary>
/// �������� ������ �������
/// </summary>
/// <param name="researcheIds"></param>
/// <param name="createdDate"></param>
/// <param name="fio"></param>
/// <returns></returns>
app.MapGet("/GetOrderList", async ([FromServices] Repository repository) =>
    (await repository.GetOrderListAsync()).Select(x => new OrderDto
    {
        Id = x.Id,
        FIO = x.FIO
        
    }).ToList())
    .WithName("GetOrderList")
    .WithOpenApi();


app.Run();
